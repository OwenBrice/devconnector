import express from 'express'
import connectDB from '../config/db'
import authRoute from './routes/api/auth'
import usersRoute from './routes/api/users'
import profileRoute from './routes/api/profile'
import postsRoute from './routes/api/posts'

const app = express()

// Connect Database
connectDB()

// Init Middlware
app.use(express.json({ extended: false }))

app.get('/', (req, res) => res.send('API Running'))

// Define Routes
app.use('/api/auth', authRoute)
app.use('/api/users', usersRoute)
app.use('/api/profile', profileRoute)
app.use('/api/posts', postsRoute)

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
